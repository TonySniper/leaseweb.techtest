﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LeaseWeb.TechTest.DataAccess;
using LeaseWeb.TechTest.DataAccess.Model;
using LeaseWeb.TechTest.ViewModels.CustomerVM;
using LeaseWeb.TechTest.Application;

namespace LeaseWeb.TechTest.Web.Controllers
{
    public class CustomersController : Controller
    {
        private readonly Context _context;
        private readonly ICustomerApplicationService _customerApplicationService;

        public CustomersController(Context context, ICustomerApplicationService customerApplicationService)
        {
            _customerApplicationService = customerApplicationService;
            _context = context;
        }

        // GET: Customers
        public async Task<IActionResult> Index()
        {
            var vm = _customerApplicationService.GetCustomers();

            return View(vm);
        }

        // GET: Customers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var customer = await _context.Customers
            //    .FirstOrDefaultAsync(m => m.Id == id);
            var customer = _customerApplicationService.GetCustomerById(id.Value);

            if (customer == null)
            {
                return NotFound();
            }

            this.CreateTempDataRedirect("Customers", "Details", id.Value);

            return View(customer);
        }


        // GET: Customers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("Id,CompanyName,RegistrationNumber,WebSite,NumberOfEmployees,MaxOutstandingOrderAmount,CustomerSince")] CreateCustomerVM customer)
        public async Task<IActionResult> Create(CreateCustomerVM vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _customerApplicationService.AddCustomer(vm);

                    return RedirectToAction(nameof(Index));
                }
                return View(vm);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(vm);
            }
        }

        // GET: Customers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vm = _customerApplicationService.GetCustomerById(id.Value);

            if (vm == null)
            {
                return NotFound();
            }

            TempData["customerId"] = id.Value;

            this.CreateTempDataRedirect("Customers", "Edit", id.Value);

            return View(vm);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditCustomerVM vm)
        {
            try
            {
                if (id != vm.Customer.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    _customerApplicationService.UpdateCustomer(vm);
                    
                    return RedirectToAction(nameof(Index));
                }
                return View(vm);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(vm);
            }
        }

        // GET: Customers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = _customerApplicationService.GetCustomerById(id.Value);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _customerApplicationService.DeleteCustomer(id);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost, ActionName("Search")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Search(CustomerSearchRequestVM vm)
        {
            var result = _customerApplicationService.GetCustomers(vm.CustomerName, vm.RegistrationNumber, vm.ContactEmail, vm.ContactAddress);

            return View("Index", result);
        }

        private void CreateTempDataRedirect(string controller, string action, int routeValues)
        {
            TempData["controller"] = controller;
            TempData["action"] = action;
            TempData["routeValues"] = routeValues;
        }

        private void RedirectToTempDataRoute()
        {
            //TempData["controller"] = controller;
            //TempData["action"] = action;
            //TempData["routeValues"] = routeValues;

            var controller = TempData["controller"].ToString();
            var action = TempData["action"].ToString();
            var routeValues = (int)TempData["routeValues"];

            RedirectToAction(action, controller, routeValues);
        }
    }
}
