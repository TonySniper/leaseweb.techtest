﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LeaseWeb.TechTest.DataAccess;
using LeaseWeb.TechTest.DataAccess.Model;
using LeaseWeb.TechTest.Application;
using LeaseWeb.TechTest.ViewModels.ContactVM;
using LeaseWeb.TechTest.ViewModels.CustomerVM;

namespace LeaseWeb.TechTest.Web.Controllers
{
    public class ContactsController : Controller
    {
        private readonly Context _context;
        private readonly IContactApplicationService _contactApplicationService;

        public ContactsController(Context context, IContactApplicationService contactApplicationService)
        {
            _context = context;
            _contactApplicationService = contactApplicationService;
        }


        public IActionResult SetMainContact(int contactId)
        {
            try
            {
                _contactApplicationService.SetContactAsMainContact(contactId);
                return RedirectToAction("Edit", contactId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: Contacts
        public async Task<IActionResult> Index()
        {
            this.VoidTempDataRedirect();

            var result = _contactApplicationService.GetContacts();

            return View(result);
        }

        // GET: Contacts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = await _context.Contacts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (contact == null)
            {
                return NotFound();
            }


            this.CreateTempDataRedirect("Contacts", "Details", id.Value);
            return View(contact);
        }

        // GET: Contacts/Create
        public IActionResult Create()
        {
            var customerId = (int?)TempData.Peek("customerId");
            //var customerId = (int?)TempData["customerId"];

            if (!customerId.HasValue)
            {
                throw new Exception("Invalid customer ID");
            }

            var vm = new ContactVM
            {
                CustomerId = customerId.Value
            };

            //TempData["customerId"] = customerId.Value;

            return View(vm);
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ContactVM vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _contactApplicationService.AddContact(vm);
                    //return RedirectToAction(nameof(Index));
                    if (this.IsTempDataRedirect())
                        return this.RedirectToTempDataRoute();
                    return RedirectToAction(nameof(Index));
                    //return RedirectToAction(redirectAction, redirectController, redirectRouteValues);
                }
                return View(vm);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(vm);
            }
        }

        // GET: Contacts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = _contactApplicationService.GetContactById(id.Value);
            if (contact == null)
            {
                return NotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditContactVM vm)
        {
            if (id != vm.Contact.Id)
            {
                return NotFound();
            }
            try
            {
                if (ModelState.IsValid)
                {
                    _contactApplicationService.UpdateContact(vm);
                    if (this.IsTempDataRedirect())
                        return this.RedirectToTempDataRoute();
                    return RedirectToAction(nameof(Index));
                }
                return View(vm);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(vm);
            }
        }

        // GET: Contacts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = _contactApplicationService.GetContactById(id.Value);
            if (contact == null)
            {
                return NotFound();
            }

            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _contactApplicationService.DeleteContact(id);

            if (this.IsTempDataRedirect())
                return this.RedirectToTempDataRoute();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost, ActionName("Search")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Search(CustomerSearchRequestVM vm)
        {
            var result = _contactApplicationService.GetContacts(vm.ContactName, vm.RegistrationNumber, vm.ContactEmail, vm.ContactAddress, vm.CustomerName);

            return View("Index", result);
        }

        private void CreateTempDataRedirect(string controller, string action, int routeValues)
        {
            TempData["controller"] = controller;
            TempData["action"] = action;
            TempData["routeValues"] = routeValues;
        }

        private IActionResult RedirectToTempDataRoute()
        {
            var controller = TempData["controller"].ToString();
            var action = TempData["action"].ToString();
            var routeValues = (int)TempData["routeValues"];

            return RedirectToAction(action, controller, new { id = routeValues });
        }

        private bool IsTempDataRedirect()
        {
            return TempData["controller"] != null && TempData["action"] != null && TempData["routeValues"] != null;
        }

        private void VoidTempDataRedirect()
        {
            TempData.Clear();
        }
    }
}
