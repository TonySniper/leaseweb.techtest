﻿using LeaseWeb.TechTest.ViewModels.ContactVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.Application
{
    public interface IContactApplicationService
    {
        void SetContactAsMainContact(int contactId);
        IEnumerable<ContactSearchResponseVM> GetContacts(string contactName = "", string registrationNumber = "", string contactEmail = "", string contactAddress = "", string companyName = "");
        int UpdateContact(EditContactVM vm);
        int AddContact(ContactVM vm);
        EditContactVM GetContactById(int id);
        void DeleteContact(int id);
        //void DeleteContact(int id);
        //void Addcontact(CreateContactVM vm);
    }
}
