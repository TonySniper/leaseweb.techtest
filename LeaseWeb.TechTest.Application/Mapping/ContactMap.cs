﻿using LeaseWeb.TechTest.DataAccess.Model;
using LeaseWeb.TechTest.ViewModels.ContactVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.Application.Mapping
{
    public class ContactMap
    {
        public Contact MapToEntity(ContactVM vm)
        {
            var contact = new Contact
            {
                Address = vm.Address,
                City = vm.City,
                CustomerId = vm.CustomerId,
                Email = vm.Email,
                Id = vm.Id,
                IsFinancial = vm.IsFinancial,
                IsMain = vm.IsMain,
                IsOther = vm.IsOther,
                IsTechnical = vm.IsTechnical,
                Name = vm.Name,
                PhoneNumber = vm.PhoneNumber
            };

            return contact;
        }

        public ContactVM MapToVM(Contact contact)
        {
            var vm = new ContactVM
            {
                Address = contact.Address,
                City = contact.City,
                CustomerId = contact.CustomerId,
                Email = contact.Email,
                Id = contact.Id,
                IsFinancial = contact.IsFinancial,
                IsMain = contact.IsMain,
                IsOther = contact.IsOther,
                IsTechnical = contact.IsTechnical,
                Name = contact.Name,
                PhoneNumber = contact.PhoneNumber
            };

            return vm;
        }
    }
}
