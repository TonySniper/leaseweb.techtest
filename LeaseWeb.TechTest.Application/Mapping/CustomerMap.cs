﻿using LeaseWeb.TechTest.DataAccess.Model;
using LeaseWeb.TechTest.ViewModels.CustomerVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.Application.Mapping
{
    public class CustomerMap
    {
        public Customer MapToEntity(CustomerVM vm)
        {
            var customer = new Customer
            {
                Id = vm.Id,
                CompanyName = vm.CompanyName,
                CustomerSince = vm.CustomerSince,
                MaxOutstandingOrderAmount = vm.MaxOutstandingOrderAmount,
                NumberOfEmployees = vm.NumberOfEmployees,
                RegistrationNumber = vm.RegistrationNumber,
                WebSite = vm.WebSite
            };

            return customer;
        }

        public CustomerVM MapToVM(Customer customer)
        {
            var vm = new CustomerVM
            {
                CompanyName = customer.CompanyName,
                CustomerSince = customer.CustomerSince,
                Id = customer.Id,
                MaxOutstandingOrderAmount = customer.MaxOutstandingOrderAmount,
                NumberOfEmployees = customer.NumberOfEmployees,
                RegistrationNumber = customer.RegistrationNumber,
                WebSite = customer.WebSite
            };

            return vm;
        }
    }
}
