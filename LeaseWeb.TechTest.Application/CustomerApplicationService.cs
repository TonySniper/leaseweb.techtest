﻿using LeaseWeb.TechTest.Application.Mapping;
using LeaseWeb.TechTest.Core.CoreServices;
using LeaseWeb.TechTest.ViewModels.ContactVM;
using LeaseWeb.TechTest.ViewModels.CustomerVM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LeaseWeb.TechTest.Application
{
    public class CustomerApplicationService : ICustomerApplicationService
    {
        private ICustomerCoreService _customerCoreService;
        private IContactCoreService _contactCoreService;
        private CustomerMap _customerMapper;
        private ContactMap _contactMapper;

        public CustomerApplicationService(ICustomerCoreService customerCoreService, IContactCoreService contactCoreService)
        {
            _customerCoreService = customerCoreService;
            _contactCoreService = contactCoreService;
            _customerMapper = new CustomerMap();
            _contactMapper = new ContactMap();
        }

        public int AddCustomer(CreateCustomerVM vm)
        {
            vm.ValidateContactPhoneNumberRequired();

            var customer = _customerMapper.MapToEntity(vm.Customer);
            var contact = _contactMapper.MapToEntity(vm.Contact);

            var createdCustomer = _customerCoreService.AddOrUpdateCustomer(customer);

            contact.CustomerId = createdCustomer.Id;
            contact.PhoneNumber = vm.RequiredPhoneNumber;

            _contactCoreService.AddOrUpdateContact(contact);

            return createdCustomer.Id;
        }

        public int UpdateCustomer(EditCustomerVM vm)
        {
            var customer = _customerMapper.MapToEntity(vm.Customer);
            return _customerCoreService.AddOrUpdateCustomer(customer).Id;
        }

        public void DeleteCustomer(int id)
        {
            _customerCoreService.DeleteCustomer(id);
        }

        public IEnumerable<CustomerSearchResultVM> GetCustomers(string customerName = "", string registrationNumber = "", string contactEmail = "", string contactAddress = "")
        {
            var customers = _customerCoreService.GetCustomers(customerName, registrationNumber);
            var result = new List<CustomerSearchResultVM>();

            foreach (var item in customers)
            {
                var contact = _contactCoreService.GetContacts().FirstOrDefault(x => x.IsMain == true && x.CustomerId == item.Id);

                var customerVm = _customerMapper.MapToVM(item);

                var vm = new CustomerSearchResultVM
                {
                    Customer = customerVm,
                    MainContactAddress = contact?.Address ?? string.Empty,
                    MainContactEmail = contact?.Email ?? string.Empty
                };

                result.Add(vm);
            }

            return result;
        }

        public EditCustomerVM GetCustomerById(int id)
        {
            var customer = _customerCoreService.GetCustomers().FirstOrDefault(x => x.Id == id);

            if (customer == null)
                return null;

            var contacts = _contactCoreService.GetContacts().Where(x => x.CustomerId == customer.Id);

            var contactList = new List<ContactVM>();

            foreach (var item in contacts)
            {
                var contactVm = _contactMapper.MapToVM(item);
                contactList.Add(contactVm);
            }

            var customerVm = _customerMapper.MapToVM(customer);

            var vm = new EditCustomerVM
            {
                Contacts = contactList,
                Customer = customerVm
            };

            return vm;
        }
    }
}
