﻿using LeaseWeb.TechTest.Application.Mapping;
using LeaseWeb.TechTest.Core.CoreServices;
using LeaseWeb.TechTest.ViewModels.ContactVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeaseWeb.TechTest.Application
{
    public class ContactApplicationService : IContactApplicationService
    {
        private ICustomerCoreService _customerCoreService;
        private IContactCoreService _contactCoreService;
        private CustomerMap _customerMapper;
        private ContactMap _contactMapper;

        public ContactApplicationService(ICustomerCoreService customerCoreService, IContactCoreService contactCoreService)
        {
            _customerCoreService = customerCoreService;
            _contactCoreService = contactCoreService;
            _customerMapper = new CustomerMap();
            _contactMapper = new ContactMap();
        }

        public IEnumerable<ContactSearchResponseVM> GetContacts(string contactName = "", string registrationNumber = "", string contactEmail = "", string contactAddress = "", string companyName = "")
        {
            var contacts = _contactCoreService.GetContacts(contactName, contactAddress, contactEmail);
            var result = new List<ContactSearchResponseVM>();

            foreach (var item in contacts)
            {
                var customer = _customerCoreService.GetCustomers().FirstOrDefault(x => x.Id == item.CustomerId);

                var contactVm = _contactMapper.MapToVM(item);

                var vm = new ContactSearchResponseVM
                {
                    Contact = contactVm,
                    CustomerCompanyName = customer.CompanyName,
                    CustomerRegistrationNumber = customer.RegistrationNumber
                };

                result.Add(vm);
            }

            if (!string.IsNullOrWhiteSpace(companyName))
                result = result.Where(x => x.CustomerCompanyName.Contains(companyName, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (!string.IsNullOrWhiteSpace(registrationNumber))
                result = result.Where(x => x.CustomerRegistrationNumber.Contains(registrationNumber, StringComparison.InvariantCultureIgnoreCase)).ToList();

            return result;
        }

        public EditContactVM GetContactById(int id)
        {
            var contact = _contactCoreService.GetContacts().FirstOrDefault(x => x.Id == id);

            if (contact == null)
                return null;

            var vm = _contactMapper.MapToVM(contact);

            return new EditContactVM
            {
                Contact = vm
            };
        }

        public void SetContactAsMainContact(int contactId)
        {
            _contactCoreService.SetMainContact(contactId);
        }

        public int UpdateContact(EditContactVM vm)
        {
            vm.Contact.ValidateContactPhoneNumberRequired();

            var contact = _contactMapper.MapToEntity(vm.Contact);

            if (contact.IsMain)
            {
                _contactCoreService.SetMainContact(vm.Contact.Id, vm.Contact.PhoneNumber);
            }
            return _contactCoreService.AddOrUpdateContact(contact).Id;            
        }

        public int AddContact(ContactVM vm)
        {
            vm.ValidateContactPhoneNumberRequired();
            var contact = _contactMapper.MapToEntity(vm);

            return _contactCoreService.AddOrUpdateContact(contact).Id;
        }

        public void DeleteContact(int id)
        {
            _contactCoreService.DeleteContact(id);
        }
    }
}
