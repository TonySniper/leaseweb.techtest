﻿using LeaseWeb.TechTest.ViewModels.CustomerVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.Application
{
    public interface ICustomerApplicationService
    {
        int AddCustomer(CreateCustomerVM vm);
        int UpdateCustomer(EditCustomerVM vm);
        void DeleteCustomer(int id);
        IEnumerable<CustomerSearchResultVM> GetCustomers(string customerName = "", string registrationNumber = "", string contactEmail = "", string contactAddress = "");
        EditCustomerVM GetCustomerById(int id);
    }
}
