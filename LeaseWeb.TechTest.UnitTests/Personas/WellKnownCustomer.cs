﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas
{
    public abstract class WellKnownCustomer
    {
        //public abstract int Id { get; set; }
        public abstract string CompanyName { get; }
        public abstract string RegistrationNumber { get; }
        public abstract string WebSite { get; }
        public abstract int NumberOfEmployees { get; }
        public abstract int MaxOutstandingOrderAmount { get; }
        public abstract DateTime CustomerSince { get; }

        //public virtual IList<Contact> Contacts { get; set; }

    }
}
