﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas
{
    public class StandardContact : WellKnownContact
    {
        public override string Name => "John Johnson";

        public override string City => "Madrid";

        public override string Address => "Saint Street";

        public override string PhoneNumber => "+33 962 3598";

        public override string Email => "johnjohnson@gmail.com";

        public override bool IsMain => false;

        public override bool IsFinancial => false;

        public override bool IsTechnical => false;

        public override bool IsOther => false;

        public override int CustomerId => 25;
    }
}
