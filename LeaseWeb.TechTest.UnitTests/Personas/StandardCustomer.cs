﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas
{
    public class StandardCustomer : WellKnownCustomer
    {
        public override string CompanyName => "Auto Parts Limited";

        public override string RegistrationNumber => "0123456789";

        public override string WebSite => "www.dummysite.com";

        public override int NumberOfEmployees => 10;

        public override int MaxOutstandingOrderAmount => 15000;

        public override DateTime CustomerSince => DateTime.UtcNow.Date;
    }
}
