﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas
{
    public class StandardContactUpdated : WellKnownContact
    {
        public override string Name => "John Johnson UPDATED";

        public override string City => "Limerick";

        public override string Address => "Tree Avenue";

        public override string PhoneNumber => "+55 11 95464 3215";

        public override string Email => "updatedjohn@gmail.com";

        public override bool IsMain => false;

        public override bool IsFinancial => false;

        public override bool IsTechnical => false;

        public override bool IsOther => false;

        public override int CustomerId => 25;
    }
}
