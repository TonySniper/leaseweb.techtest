﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas
{
    public class StandardCustomerUpdated : WellKnownCustomer
    {
        public override string CompanyName => "Auto Parts Limited UPDATED";

        public override string RegistrationNumber => "3336669990";

        public override string WebSite => "www.updated.com";

        public override int NumberOfEmployees => 17;

        public override int MaxOutstandingOrderAmount => 25000;

        public override DateTime CustomerSince => DateTime.Now.Date.AddDays(-1);
    }
}
