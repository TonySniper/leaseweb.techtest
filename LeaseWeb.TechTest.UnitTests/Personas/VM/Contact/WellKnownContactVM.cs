﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas.VM.Contact
{
    public abstract class WellKnownContactVM
    {
        public abstract int Id { get; }
        public abstract string Name { get; }
        public abstract string City { get; }
        public abstract string Address { get; }
        public abstract string PhoneNumber { get; }
        public abstract string Email { get; }
        public abstract bool IsMain { get; }
        public abstract bool IsFinancial { get; }
        public abstract bool IsTechnical { get; }
        public abstract bool IsOther { get; }
        public abstract int CustomerId { get; }

        //public void ValidateContactPhoneNumberRequired()
        //{
        //    bool isPhoneNumberRequired = this.IsFinancial || this.IsMain || this.IsTechnical;

        //    if (isPhoneNumberRequired && string.IsNullOrWhiteSpace(this.PhoneNumber))
        //        throw new Exception("Phone number is required for Main, Technical and Financial contacts");
        //}
    }
}
