﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas.VM.Contact
{
    public class StandardContactVM : WellKnownContactVM
    {
        public override int Id => 0;
        public override string Name => "John Contact";
        public override string City => "Dublin";
        public override string Address => "Saint Street";
        public override string PhoneNumber => "+55 11 95656 5656";
        public override string Email => "john@email.com";
        public override bool IsMain => false;
        public override bool IsFinancial => false;
        public override bool IsTechnical => false;
        public override bool IsOther => false;
        public override int CustomerId => 1;
    }
}
