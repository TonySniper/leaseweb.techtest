﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas.VM.Customer
{
    public abstract class WellKnownCustomerVM
    {
        public abstract int Id { get; }
        public abstract string CompanyName { get; }
        public abstract string RegistrationNumber { get; }
        public abstract string WebSite { get; }
        public abstract int NumberOfEmployees { get; }
        public abstract int MaxOutstandingOrderAmount { get; }
        public abstract DateTime CustomerSince { get; }
    }
}
