﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Personas.VM.Customer
{
    public class StandardCustomerVM : WellKnownCustomerVM
    {
        public override int Id => 0;

        public override string CompanyName => "Dummy Company VM";

        public override string RegistrationNumber => "0123456789";

        public override string WebSite => "www.dummy.com";

        public override int NumberOfEmployees => 10;

        public override int MaxOutstandingOrderAmount => 1500;

        public override DateTime CustomerSince => DateTime.Now.Date;
    }
}
