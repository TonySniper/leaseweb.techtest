﻿using LeaseWeb.TechTest.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests
{
    public abstract class UnitTestBase
    {
        protected Context _context;

        protected UnitTestBase()
        {
            _context = this.CreateContext();
        }

        private Context CreateContext()
        {
            var randomDatabaseName = Guid.NewGuid().ToString();
            var dbOptions = new DbContextOptionsBuilder<Context>()
                                .UseInMemoryDatabase(randomDatabaseName)
                                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                .Options;

            return new Context(dbOptions, true);
        }
    }
}
