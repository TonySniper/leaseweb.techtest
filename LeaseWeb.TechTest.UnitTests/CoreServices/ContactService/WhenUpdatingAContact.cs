﻿using LeaseWeb.TechTest.Core.CoreServices;
using LeaseWeb.TechTest.UnitTests.Builders;
using LeaseWeb.TechTest.UnitTests.Personas;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.CoreServices.ContactService
{
    [TestClass]
    public class WhenUpdatingAContact : UnitTestBase
    {
        private ICustomerCoreService _customerService;
        private IContactCoreService _contactService;

        public WhenUpdatingAContact()
        {
            _contactService = new ContactCoreService(base._context);
            _customerService = new CustomerCoreService(base._context);
        }

        [TestMethod]
        public void ItShouldBeUpdated()
        {
            var customerId = this.AssumeCustomerExists();
            var contactId = this.AssumeContactExists(customerId, true);

            var contactUpdated = new ContactBuilder()
                    .With(new StandardContactUpdated())
                    .WithCustomerId(customerId)
                    .WithMainContactType(false)
                    .BuildEntity();

            var contact = _contactService.AddOrUpdateContact(contactUpdated);

            Assert.AreEqual(contactUpdated.Address, contact.Address);
            Assert.AreEqual(contactUpdated.City, contact.City);
            Assert.AreEqual(contactUpdated.CustomerId, contact.CustomerId);
            Assert.AreEqual(contactUpdated.Email, contact.Email);
            Assert.AreEqual(contactUpdated.Id, contact.Id);
            Assert.AreEqual(contactUpdated.IsFinancial, contact.IsFinancial);
            Assert.AreEqual(contactUpdated.IsMain, contact.IsMain);
            Assert.AreEqual(contactUpdated.IsOther, contact.IsOther);
            Assert.AreEqual(contactUpdated.IsTechnical, contact.IsTechnical);
            Assert.AreEqual(contactUpdated.Name, contact.Name);
            Assert.AreEqual(contactUpdated.PhoneNumber, contact.PhoneNumber);
        }

        [TestMethod]
        public void MainContactShouldBeUpdated()
        {
            var customerId = this.AssumeCustomerExists();
            var mainContactId = this.AssumeContactExists(customerId, true);
            var shouldBeMainContactId = this.AssumeContactExists(customerId, false, "differentEmail@email.com");

            var contact = _contactService.SetMainContact(shouldBeMainContactId);

            var previousMainContact = _context.Contacts.Find(mainContactId);
            var currentMainContact = _context.Contacts.Find(contact.Id);

            Assert.IsTrue(currentMainContact.IsMain);
            Assert.IsFalse(previousMainContact.IsMain);
        }
        
        private int AssumeContactExists(int customerId, bool isMainContact, string email = "")
        {
            var wellKnownContact = new StandardContact();

            var builder = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(isMainContact);

            if (!string.IsNullOrWhiteSpace(email))
                builder = builder.WithEmail(email);

            var contact = builder.BuildEntity();

            return _contactService.AddOrUpdateContact(contact).Id;
        }

        private int AssumeCustomerExists(string registrationNumber = "0123456789")
        {
            var wellKnownCustomer = new StandardCustomer();
            var customer = new CustomerBuilder()
                    .With(wellKnownCustomer)
                    .WithRegistrationNumber(registrationNumber)
                    .BuildEntity();

            return _customerService.AddOrUpdateCustomer(customer).Id;
        }
    }
}
