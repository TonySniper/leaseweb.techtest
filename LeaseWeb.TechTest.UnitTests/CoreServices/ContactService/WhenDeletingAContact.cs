﻿using LeaseWeb.TechTest.Core.CoreServices;
using LeaseWeb.TechTest.DataAccess.Model;
using LeaseWeb.TechTest.UnitTests.Builders;
using LeaseWeb.TechTest.UnitTests.Personas;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.CoreServices.ContactService
{
    [TestClass]
    public class WhenDeletingAContact : UnitTestBase
    {
        private ICustomerCoreService _customerService;
        private IContactCoreService _contactService;

        public WhenDeletingAContact()
        {
            _contactService = new ContactCoreService(base._context);
            _customerService = new CustomerCoreService(base._context);
        }

        [TestMethod]
        public void ItShouldBeDeleted()
        {
            var existingContact = this.AssumeContactExists();

            var wellKnownContact = new StandardContact();

            var contactPersona = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(existingContact.CustomerId)
                    .WithEmail("dummy@email.com")
                    .WithMainContactType(false)
                    .BuildEntity();

            var contactToDeleteId = _contactService.AddOrUpdateContact(contactPersona).Id;

            _contactService.DeleteContact(contactToDeleteId);

            var contact = base._context.Contacts.Find(contactToDeleteId);

            Assert.IsNull(contact);
        }
        
        private Contact AssumeContactExists()
        {
            var customerId = this.AssumeCustomerExists();
            var wellKnownContact = new StandardContact();

            var contact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(true)
                    .BuildEntity();

            return _contactService.AddOrUpdateContact(contact);
        }

        private int AssumeCustomerExists(string registrationNumber = "0123456789")
        {
            var wellKnownCustomer = new StandardCustomer();
            var customer = new CustomerBuilder()
                    .With(wellKnownCustomer)
                    .WithRegistrationNumber(registrationNumber)
                    .BuildEntity();

            return _customerService.AddOrUpdateCustomer(customer).Id;
        }
    }
}
