﻿using LeaseWeb.TechTest.Core.CoreServices;
using LeaseWeb.TechTest.UnitTests.Builders;
using LeaseWeb.TechTest.UnitTests.Personas;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.CoreServices.ContactService
{
    [TestClass]
    public class WhenCreatingAContact : UnitTestBase
    {
        private IContactCoreService _contactService;
        private ICustomerCoreService _customerService;

        public WhenCreatingAContact()
        {
            _contactService = new ContactCoreService(base._context);
            _customerService = new CustomerCoreService(base._context);
        }

        [TestMethod]
        public void ItShouldBeCreated()
        {
            var customerId = this.AssumeCustomerExists();
            var isMainContact = true;
            var isTechnicalContact = true;
            var isFinancialContact = true;

            var wellKnownContact = new StandardContact();

            var contact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(isMainContact)
                    .WithTechnicalContactType(isTechnicalContact)
                    .WithFinancialContactType(isFinancialContact)
                    .BuildEntity();

            var contactOnDb = _contactService.AddOrUpdateContact(contact);

            Assert.IsTrue(contactOnDb.Id > 0);
            Assert.AreEqual(wellKnownContact.Address, contactOnDb.Address);
            Assert.AreEqual(wellKnownContact.City, contactOnDb.City);
            Assert.AreEqual(wellKnownContact.Email, contactOnDb.Email);
            Assert.AreEqual(isFinancialContact, contactOnDb.IsFinancial);
            Assert.AreEqual(isMainContact, contactOnDb.IsMain);
            Assert.AreEqual(isTechnicalContact, contactOnDb.IsTechnical);
            Assert.AreEqual(wellKnownContact.IsOther, contactOnDb.IsOther);
            Assert.AreEqual(wellKnownContact.Name, contactOnDb.Name);
            Assert.AreEqual(wellKnownContact.PhoneNumber, contactOnDb.PhoneNumber);
        }

        [TestMethod]
        public void GivenCustomerAlreadyHaveAMainContactItShouldThrowAnException()
        {
            var customerId = this.AssumeCustomerExists();
            var isMainContact = true;

            var wellKnownContact = new StandardContact();

            var firstMainContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(isMainContact)
                    .BuildEntity();

            var secondMainContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(isMainContact)
                    .BuildEntity();

            _contactService.AddOrUpdateContact(firstMainContact);

            var exception = Assert.ThrowsException<Exception>(() => _contactService.AddOrUpdateContact(secondMainContact));
            var expectedExceptionMessage = "Can not have more than one main contact";

            Assert.AreEqual(expectedExceptionMessage, exception.Message);
        }

        [TestMethod]
        public void GivenCustomerAlreadyHaveATechnicalContactItShouldThrowAnException()
        {
            var customerId = this.AssumeCustomerExists();

            var wellKnownContact = new StandardContact();

            var firstTechnicalContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(true)
                    .WithTechnicalContactType(true)
                    .BuildEntity();

            var secondTechnicalContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(false)
                    .WithTechnicalContactType(true)
                    .BuildEntity();

            _contactService.AddOrUpdateContact(firstTechnicalContact);

            var exception = Assert.ThrowsException<Exception>(() => _contactService.AddOrUpdateContact(secondTechnicalContact));
            var expectedExceptionMessage = "Can not have more than one technical contact";

            Assert.AreEqual(expectedExceptionMessage, exception.Message);
        }

        [TestMethod]
        public void GivenCustomerAlreadyHaveAFinancialContactItShouldThrowAnException()
        {
            var customerId = this.AssumeCustomerExists();

            var wellKnownContact = new StandardContact();

            var firstContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(true)
                    .WithFinancialContactType(true)
                    .BuildEntity();

            var secondContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(false)
                    .WithFinancialContactType(true)
                    .BuildEntity();

            _contactService.AddOrUpdateContact(firstContact);

            var exception = Assert.ThrowsException<Exception>(() => _contactService.AddOrUpdateContact(secondContact));
            var expectedExceptionMessage = "Can not have more than one financial contact";

            Assert.AreEqual(expectedExceptionMessage, exception.Message);
        }

        [TestMethod]
        public void GivenAContactEmailAlreadyExistsForThisCustomerItShouldThrowAnException()
        {
            var firstCustomerId = this.AssumeCustomerExists();
            //var secondCustomerId = this.AssumeCustomerExists();

            var wellKnownContact = new StandardContact();

            var firstContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(firstCustomerId)
                    .WithMainContactType(true)
                    .WithEmail("john@gmail.com")
                    .BuildEntity();

            var secondContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(firstCustomerId)
                    .WithEmail("JOHN@GMAIL.COM")
                    .BuildEntity();

            _contactService.AddOrUpdateContact(firstContact);

            var exception = Assert.ThrowsException<Exception>(() => _contactService.AddOrUpdateContact(secondContact));
            var expectedExceptionMessage = "Email of the contact for this customer must be unique";

            Assert.AreEqual(expectedExceptionMessage, exception.Message);
        }

        [TestMethod]
        public void GivenAContactEmailAlreadyExistsForAnotherCustomerItShouldBeCreated()
        {
            var firstCustomerId = this.AssumeCustomerExists("2222211111");
            var secondCustomerId = this.AssumeCustomerExists("3333377777");
            var isMainContact = true;
            var emailAddress = "john@gmail.com";

            var wellKnownContact = new StandardContact();

            var firstContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(firstCustomerId)
                    .WithMainContactType(isMainContact)
                    .WithEmail(emailAddress)
                    .BuildEntity();

            var secondContact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(secondCustomerId)
                    .WithMainContactType(isMainContact)
                    .WithEmail(emailAddress)
                    .BuildEntity();

            _contactService.AddOrUpdateContact(firstContact);
            var contactOnDb = _contactService.AddOrUpdateContact(secondContact);

            Assert.IsTrue(contactOnDb.Id > 0);
            Assert.AreEqual(wellKnownContact.Address, contactOnDb.Address);
            Assert.AreEqual(wellKnownContact.City, contactOnDb.City);
            Assert.AreEqual(emailAddress, contactOnDb.Email);
            Assert.AreEqual(wellKnownContact.IsFinancial, contactOnDb.IsFinancial);
            Assert.AreEqual(isMainContact, contactOnDb.IsMain);
            Assert.AreEqual(wellKnownContact.IsTechnical, contactOnDb.IsTechnical);
            Assert.AreEqual(wellKnownContact.IsOther, contactOnDb.IsOther);
            Assert.AreEqual(wellKnownContact.Name, contactOnDb.Name);
            Assert.AreEqual(wellKnownContact.PhoneNumber, contactOnDb.PhoneNumber);
        }

        private int AssumeCustomerExists(string registrationNumber = "0123456789")
        {
            var wellKnownCustomer = new StandardCustomer();
            var customer = new CustomerBuilder()
                    .With(wellKnownCustomer)
                    .WithRegistrationNumber(registrationNumber)
                    .BuildEntity();

            return _customerService.AddOrUpdateCustomer(customer).Id;
        }
    }
}
