﻿using LeaseWeb.TechTest.Core.CoreServices;
using LeaseWeb.TechTest.UnitTests.Builders;
using LeaseWeb.TechTest.UnitTests.Personas;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.CoreServices.CustomerService
{
    [TestClass]
    public class WhenUpdatingACustomer : UnitTestBase
    {
        private ICustomerCoreService _customerService;

        public WhenUpdatingACustomer()
        {
            _customerService = new CustomerCoreService(base._context);
        }

        [TestMethod]
        public void ItShouldBeUpdated()
        {
            var customerId = this.AssumeCustomerExists();

            var customerPersona = new StandardCustomerUpdated();

            var customerUpdated = new CustomerBuilder()
                    .With(customerPersona)
                    .WithId(customerId)
                    .BuildEntity();

            var customer = _customerService.AddOrUpdateCustomer(customerUpdated);

            Assert.AreEqual(customerId, customer.Id);
            Assert.AreEqual(customerPersona.CompanyName, customer.CompanyName);
            Assert.AreEqual(customerPersona.CustomerSince, customer.CustomerSince);
            Assert.AreEqual(customerPersona.MaxOutstandingOrderAmount, customer.MaxOutstandingOrderAmount);
            Assert.AreEqual(customerPersona.NumberOfEmployees, customer.NumberOfEmployees);
            Assert.AreEqual(customerPersona.RegistrationNumber, customer.RegistrationNumber);
            Assert.AreEqual(customerPersona.WebSite, customer.WebSite);
        }

        [TestMethod]
        public void GivenACustomerHasNumberOfEmployeesLessThan1ItShouldThrowAnException()
        {
            var customerId = this.AssumeCustomerExists();

            var customerPersona = new StandardCustomerUpdated();

            var customerUpdated = new CustomerBuilder()
                    .With(customerPersona)
                    .WithId(customerId)
                    .WithNumberOfEmployees(0)
                    .BuildEntity();

            var exception = Assert.ThrowsException<Exception>(() => _customerService.AddOrUpdateCustomer(customerUpdated));

            var expectedMessage = "Number of employees must be at least 1";

            Assert.AreEqual(expectedMessage, exception.Message);
        }

        private int AssumeCustomerExists()
        {
            var customerPersona = new StandardCustomer();

            var customer = new CustomerBuilder()
                    .With(customerPersona)
                    .BuildEntity();

            var customerOnDb = _customerService.AddOrUpdateCustomer(customer);

            return customerOnDb.Id;
        }
    }
}
