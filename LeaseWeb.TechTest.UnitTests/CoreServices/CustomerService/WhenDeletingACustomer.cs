﻿using LeaseWeb.TechTest.Core.CoreServices;
using LeaseWeb.TechTest.UnitTests.Builders;
using LeaseWeb.TechTest.UnitTests.Personas;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.CoreServices.CustomerService
{
    [TestClass]
    public class WhenDeletingACustomer : UnitTestBase
    {
        private ICustomerCoreService _customerService;
        private IContactCoreService _contactService;

        public WhenDeletingACustomer()
        {
            _contactService = new ContactCoreService(base._context);
            _customerService = new CustomerCoreService(base._context);
        }

        [TestMethod]
        public void ItShouldBeDeleted()
        {
            var customerId = this.AssumeCustomerExists();

            _customerService.DeleteCustomer(customerId);

            var customer = base._context.Customers.Find(customerId);

            Assert.IsNull(customer);
        }

        [TestMethod]
        public void ItShouldDeleteAllItsRelatedContacts()
        {
            var customerId = this.AssumeCustomerExists();
            var contactId = this.AssumeContactExists(customerId);

            _customerService.DeleteCustomer(customerId);

            var customer = _context.Customers.Find(customerId);
            var contact = _context.Contacts.Find(contactId);

            Assert.IsNull(customer);
            Assert.IsNull(contact);
        }

        public int AssumeCustomerExists()
        {
            var customerPersona = new StandardCustomer();

            var customer = new CustomerBuilder()
                    .With(customerPersona)
                    .BuildEntity();

            var customerOnDb = _customerService.AddOrUpdateCustomer(customer);

            return customerOnDb.Id;
        }

        public int AssumeContactExists(int customerId)
        {
            //var customerId = this.AssumeCustomerExists();
            var wellKnownContact = new StandardContact();

            var contact = new ContactBuilder()
                    .With(wellKnownContact)
                    .WithCustomerId(customerId)
                    .WithMainContactType(true)
                    .BuildEntity();

            return _contactService.AddOrUpdateContact(contact).Id;
        }
    }
}
