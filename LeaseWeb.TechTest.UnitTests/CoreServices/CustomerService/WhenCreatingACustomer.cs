﻿using LeaseWeb.TechTest.Core.CoreServices;
using LeaseWeb.TechTest.UnitTests.Builders;
using LeaseWeb.TechTest.UnitTests.Personas;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.CoreServices.CustomerService
{
    [TestClass]
    public class WhenCreatingACustomer : UnitTestBase
    {
        private ICustomerCoreService _customerService;

        public WhenCreatingACustomer()
        {
            _customerService = new CustomerCoreService(base._context);
        }

        [TestMethod]
        public void ItShouldBeCreated()
        {
            var customerPersona = new StandardCustomer();

            var customer = new CustomerBuilder()
                    .With(customerPersona)
                    .BuildEntity();

            var customerOnDb = _customerService.AddOrUpdateCustomer(customer);

            Assert.IsTrue(customer.Id > 0);
            Assert.AreEqual(customerPersona.CompanyName, customerOnDb.CompanyName);
            Assert.AreEqual(customerPersona.CustomerSince, customerOnDb.CustomerSince);
            Assert.AreEqual(customerPersona.MaxOutstandingOrderAmount, customerOnDb.MaxOutstandingOrderAmount);
            Assert.AreEqual(customerPersona.NumberOfEmployees, customerOnDb.NumberOfEmployees);
            Assert.AreEqual(customerPersona.RegistrationNumber, customerOnDb.RegistrationNumber);
            Assert.AreEqual(customerPersona.WebSite, customerOnDb.WebSite);
        }

        [TestMethod]
        public void GivenACustomerHasDuplicateRegistryNumberItShouldNotBeCreated()
        {
            var existingCustomerPersona = new StandardCustomer();

            var existingCustomer = new CustomerBuilder()
                    .With(existingCustomerPersona)
                    .WithRegistrationNumber("0000055555")
                    .BuildEntity();

            var newCustomer = new CustomerBuilder()
                    .With(existingCustomerPersona)
                    .WithRegistrationNumber("0000055555")
                    .BuildEntity();

            var existingCustomerOnDb = _customerService.AddOrUpdateCustomer(existingCustomer);
            //var newCustomerOnDb = _customerService.AddCustomer(newCustomer);

            var exception = Assert.ThrowsException<Exception>(() => _customerService.AddOrUpdateCustomer(newCustomer));
            var expectedExceptionMessage = $"Customer with registration number {newCustomer.RegistrationNumber} already exists";

            Assert.AreEqual(exception.Message, expectedExceptionMessage);
        }
        
        [TestMethod]
        public void GivenACustomerHasNumberOfEmployeesLessThan1ItShouldThrowAnException()
        {
            var customerPersona = new StandardCustomer();

            var customer = new CustomerBuilder()
                    .With(customerPersona)
                    .WithNumberOfEmployees(0)
                    .BuildEntity();

            var exception = Assert.ThrowsException<Exception>(() => _customerService.AddOrUpdateCustomer(customer));

            var expectedMessage = "Number of employees must be at least 1";

            Assert.AreEqual(expectedMessage, exception.Message);
        }
    }
}
