﻿using LeaseWeb.TechTest.DataAccess.Model;
using LeaseWeb.TechTest.UnitTests.Personas;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Builders
{
    public class ContactBuilder
    {
        private WellKnownContact _wellKnownContact;
        private bool _withCustomerId;
        private bool _withMainContactType;
        private bool _withTechnicalContactType;
        private bool _withFinancialContactType;
        private bool _withOtherContactType;
        private bool _withEmail;

        private int _customerId;
        private bool _isMainContactType;
        private bool _isTechnicalContactType;
        private bool _isFinancialContactType;
        private bool _isOtherContactType;
        private string _email;

        public ContactBuilder With(WellKnownContact wellKnownContact)
        {
            _wellKnownContact = wellKnownContact;

            return this;
        }

        public ContactBuilder WithCustomerId(int id)
        {
            _withCustomerId = true;
            _customerId = id;

            return this;
        }

        public ContactBuilder WithMainContactType(bool isMainContacType)
        {
            _withMainContactType = true;
            _isMainContactType = isMainContacType;

            return this;
        }

        public ContactBuilder WithTechnicalContactType(bool isTechnicalContacType)
        {
            _withTechnicalContactType = true;
            _isTechnicalContactType = isTechnicalContacType;

            return this;
        }

        public ContactBuilder WithFinancialContactType(bool isFinancialContacType)
        {
            _withFinancialContactType = true;
            _isFinancialContactType = isFinancialContacType;

            return this;
        }

        public ContactBuilder WithOtherContactType(bool isOtherContactType)
        {
            _withOtherContactType = true;
            _isOtherContactType = isOtherContactType;

            return this;
        }

        public ContactBuilder WithEmail(string email)
        {
            _withEmail = true;
            _email = email;

            return this;
        }

        public Contact BuildEntity()
        {
            var contact = new Contact
            {
                Address = _wellKnownContact.Address,
                City = _wellKnownContact.City,
                Email = _wellKnownContact.Email,
                CustomerId = _wellKnownContact.CustomerId,
                IsFinancial = _wellKnownContact.IsFinancial,
                IsMain = _wellKnownContact.IsMain,
                IsOther = _wellKnownContact.IsOther,
                IsTechnical = _wellKnownContact.IsTechnical,
                Name = _wellKnownContact.Name,
                PhoneNumber = _wellKnownContact.PhoneNumber
            };

            if (_withCustomerId)
                contact.CustomerId = _customerId;

            if (_withFinancialContactType)
                contact.IsFinancial = _isFinancialContactType;

            if (_withTechnicalContactType)
                contact.IsTechnical = _isTechnicalContactType;

            if (_withMainContactType)
                contact.IsMain = _isMainContactType;

            if (_withOtherContactType)
                contact.IsOther = _isOtherContactType;

            if (_withEmail)
                contact.Email = _email;

            return contact;
        }
    }
}
