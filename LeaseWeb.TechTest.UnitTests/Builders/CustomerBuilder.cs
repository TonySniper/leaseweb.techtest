﻿using LeaseWeb.TechTest.DataAccess.Model;
using LeaseWeb.TechTest.UnitTests.Personas;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Builders
{
    public class CustomerBuilder
    {
        private WellKnownCustomer _wellKnownCustomer;
        private bool _withRegistrationNumber;
        private bool _withId;
        private bool _withNumberOfEmployees;

        private string _registrationNumber;
        private int _numberOfEmployees;
        private int _id;

        public CustomerBuilder With(WellKnownCustomer wellKnownCustomer)
        {
            _wellKnownCustomer = wellKnownCustomer;
            return this;
        }

        public CustomerBuilder WithNumberOfEmployees(int number)
        {
            _withNumberOfEmployees = true;
            _numberOfEmployees = number;

            return this;
        }

        public CustomerBuilder WithRegistrationNumber(string registrationNumber)
        {
            _withRegistrationNumber = true;
            _registrationNumber = registrationNumber;

            return this;
        }

        public CustomerBuilder WithId(int id)
        {
            _withId = true;
            _id = id;

            return this;
        }

        public Customer BuildEntity()
        {
            var customer = new Customer
            {
                CompanyName = _wellKnownCustomer.CompanyName,
                CustomerSince = _wellKnownCustomer.CustomerSince,
                MaxOutstandingOrderAmount = _wellKnownCustomer.MaxOutstandingOrderAmount,
                NumberOfEmployees = _wellKnownCustomer.NumberOfEmployees,
                RegistrationNumber = _wellKnownCustomer.RegistrationNumber,
                WebSite = _wellKnownCustomer.WebSite
            };

            if (_withRegistrationNumber)
                customer.RegistrationNumber = _registrationNumber;

            if (_withId)
                customer.Id = _id;

            if (_withNumberOfEmployees)
                customer.NumberOfEmployees = _numberOfEmployees;

            return customer;
        }
    }
}
