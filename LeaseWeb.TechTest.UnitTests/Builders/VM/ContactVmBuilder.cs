﻿using LeaseWeb.TechTest.UnitTests.Personas.VM.Contact;
using LeaseWeb.TechTest.ViewModels.ContactVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Builders.VM
{
    public class ContactVmBuilder
    {
        private WellKnownContactVM wellKnownContactVM;
        private bool withMainContact;
        private bool withPhoneNumber;
        private bool withCustomerId;

        private bool isMainContact;
        private string phoneNumber;
        private int customerId;

        public ContactVmBuilder With(WellKnownContactVM wellKnownContactVM)
        {
            this.wellKnownContactVM = wellKnownContactVM;
            return this;
        }

        public ContactVmBuilder WithMainContact()
        {
            this.withMainContact = true;
            this.isMainContact = true;

            return this;
        }

        public ContactVmBuilder WithCustomerId(int id)
        {
            this.customerId = id;
            this.withCustomerId = true;

            return this;
        }

        public ContactVmBuilder WithPhoneNumber(string phoneNumber)
        {
            this.withPhoneNumber = true;
            this.phoneNumber = phoneNumber;

            return this;
        }

        public ContactVM Build()
        {
            var vm = new ContactVM
            {
                Address = this.wellKnownContactVM.Address,
                City = this.wellKnownContactVM.City,
                CustomerId = this.wellKnownContactVM.CustomerId,
                Email = this.wellKnownContactVM.Email,
                Id = this.wellKnownContactVM.Id,
                IsFinancial = this.wellKnownContactVM.IsFinancial,
                IsMain = this.wellKnownContactVM.IsMain,
                IsOther = this.wellKnownContactVM.IsOther,
                IsTechnical = this.wellKnownContactVM.IsTechnical,
                Name = this.wellKnownContactVM.Name,
                PhoneNumber = this.wellKnownContactVM.PhoneNumber
            };

            if (this.withMainContact)
                vm.IsMain = isMainContact;

            if (this.withPhoneNumber)
                vm.PhoneNumber = this.phoneNumber;

            if (this.withCustomerId)
                vm.CustomerId = this.customerId;

            return vm;
        }
    }
}
