﻿using LeaseWeb.TechTest.UnitTests.Personas.VM.Customer;
using LeaseWeb.TechTest.ViewModels.CustomerVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.Builders.VM
{
    public class CustomerVmBuilder
    {
        private WellKnownCustomerVM wellKnownCustomerVM;

        public CustomerVmBuilder With(WellKnownCustomerVM wellKnownCustomerVM)
        {
            this.wellKnownCustomerVM = wellKnownCustomerVM;
            return this;
        }

        public CustomerVM Build()
        {
            var vm = new CustomerVM
            {
                CompanyName = this.wellKnownCustomerVM.CompanyName,
                CustomerSince = this.wellKnownCustomerVM.CustomerSince,
                Id = this.wellKnownCustomerVM.Id,
                MaxOutstandingOrderAmount = this.wellKnownCustomerVM.MaxOutstandingOrderAmount,
                NumberOfEmployees = this.wellKnownCustomerVM.NumberOfEmployees,
                RegistrationNumber = this.wellKnownCustomerVM.RegistrationNumber,
                WebSite = this.wellKnownCustomerVM.WebSite
            };

            return vm;
        }
    }
}
