﻿using LeaseWeb.TechTest.DataAccess.Model;
using LeaseWeb.TechTest.UnitTests.Builders.VM;
using LeaseWeb.TechTest.UnitTests.Personas.VM.Contact;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.ApplicationServices.ContactService
{
    [TestClass]
    public class WhenCreatingAContact : ApplicationServiceUnitTestBase
    {
        [TestMethod]
        public void ItShouldBeCreated()
        {
            var customer = this.AssumeCustomerExists();

            var wellKnownContactVM = new StandardContactVM();

            var contactVm = new ContactVmBuilder()
                        .With(wellKnownContactVM)
                        .WithMainContact()
                        .WithCustomerId(customer.Id)
                        .Build();

            var id = this.contactApplicationService.AddContact(contactVm);

            var contactDbRecord = _context.Contacts.Find(id);

            Assert.IsNotNull(contactDbRecord);

            Assert.AreEqual(contactVm.IsFinancial, contactDbRecord.IsFinancial);
            Assert.AreEqual(contactVm.IsMain, contactDbRecord.IsMain);
            Assert.AreEqual(contactVm.IsOther, contactDbRecord.IsOther);
            Assert.AreEqual(contactVm.IsTechnical, contactDbRecord.IsTechnical);
            Assert.AreEqual(contactVm.Name, contactDbRecord.Name);
            Assert.AreEqual(contactVm.PhoneNumber, contactDbRecord.PhoneNumber);
        }

        private Customer AssumeCustomerExists(string registrationNumber = "0123456789")
        {
            var customer = new Customer
            {
                CompanyName = "Company name",
                CustomerSince = DateTime.Now.Date,
                MaxOutstandingOrderAmount = 1000,
                NumberOfEmployees = 10,
                RegistrationNumber = registrationNumber,
                WebSite = "Website"
            };

            _context.Customers.Add(customer);
            _context.SaveChanges();

            return customer;
        }
    }
}
