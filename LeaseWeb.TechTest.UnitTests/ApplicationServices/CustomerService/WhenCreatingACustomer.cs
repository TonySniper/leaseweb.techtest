﻿using LeaseWeb.TechTest.UnitTests.Builders.VM;
using LeaseWeb.TechTest.UnitTests.Personas.VM.Contact;
using LeaseWeb.TechTest.UnitTests.Personas.VM.Customer;
using LeaseWeb.TechTest.ViewModels.CustomerVM;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.ApplicationServices.CustomerService
{
    [TestClass]
    public class WhenCreatingACustomer : ApplicationServiceUnitTestBase
    {        
        [TestMethod]
        public void ItShouldBeCreated()
        {
            var wellKnownCustomerVM = new StandardCustomerVM();
            var wellKnownContactVM = new StandardContactVM();

            var customerVm = new CustomerVmBuilder()
                        .With(wellKnownCustomerVM)
                        .Build();

            var contactVm = new ContactVmBuilder()
                        .With(wellKnownContactVM)
                        .WithMainContact()
                        .Build();

            var phoneNumber = contactVm.PhoneNumber;
            var createCustomerVm = new CreateCustomerVM
            {
                Customer = customerVm,
                Contact = contactVm,
                RequiredPhoneNumber = phoneNumber
            };

            var id = this.customerApplicationService.AddCustomer(createCustomerVm);

            var customerDbRecord = _context.Customers.Find(id);

            Assert.IsNotNull(customerDbRecord);
            Assert.AreEqual(customerVm.CompanyName, customerDbRecord.CompanyName);
            Assert.AreEqual(customerVm.CustomerSince, customerDbRecord.CustomerSince);
            Assert.AreEqual(customerVm.MaxOutstandingOrderAmount, customerDbRecord.MaxOutstandingOrderAmount);
            Assert.AreEqual(customerVm.NumberOfEmployees, customerDbRecord.NumberOfEmployees);
            Assert.AreEqual(customerVm.RegistrationNumber, customerDbRecord.RegistrationNumber);
            Assert.AreEqual(customerVm.WebSite, customerDbRecord.WebSite);            

            var contactDbRecord = _context.Contacts.Where(x => x.CustomerId == id).FirstOrDefault();

            Assert.IsNotNull(contactDbRecord);
            Assert.AreEqual(contactVm.IsFinancial, contactDbRecord.IsFinancial);
            Assert.AreEqual(contactVm.IsMain, contactDbRecord.IsMain);
            Assert.AreEqual(contactVm.IsOther, contactDbRecord.IsOther);
            Assert.AreEqual(contactVm.IsTechnical, contactDbRecord.IsTechnical);
            Assert.AreEqual(contactVm.Name, contactDbRecord.Name);
            Assert.AreEqual(phoneNumber, contactDbRecord.PhoneNumber);
        }

        [TestMethod]
        public void GivenCustomerMainContactDoesNotHavePhoneNumberItShouldThrowException()
        {
            var wellKnownCustomerVM = new StandardCustomerVM();
            var wellKnownContactVM = new StandardContactVM();
            var emptyPhoneNumber = string.Empty;

            var customerVm = new CustomerVmBuilder()
                        .With(wellKnownCustomerVM)
                        .Build();

            var contactVm = new ContactVmBuilder()
                        .With(wellKnownContactVM)
                        .WithMainContact()
                        .WithPhoneNumber(emptyPhoneNumber)
                        .Build();

            var phoneNumber = contactVm.PhoneNumber;
            var createCustomerVm = new CreateCustomerVM
            {
                Customer = customerVm,
                Contact = contactVm,
                RequiredPhoneNumber = emptyPhoneNumber
            };

            var ex = Assert.ThrowsException<Exception>(() => this.customerApplicationService.AddCustomer(createCustomerVm));

            string expectedMessage = "Phone number is required for Main, Technical and Financial contacts";

            Assert.AreEqual(expectedMessage, ex.Message);
        }
    }
}
