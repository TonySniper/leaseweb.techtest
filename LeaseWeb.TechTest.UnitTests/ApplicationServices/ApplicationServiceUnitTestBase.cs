﻿using LeaseWeb.TechTest.Application;
using LeaseWeb.TechTest.Core.CoreServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.UnitTests.ApplicationServices
{
    public abstract class ApplicationServiceUnitTestBase : UnitTestBase
    {
        private readonly ICustomerCoreService _customerCoreService;
        private readonly IContactCoreService _contactCoreService;

        protected readonly IContactApplicationService contactApplicationService;
        protected readonly ICustomerApplicationService customerApplicationService;

        protected ApplicationServiceUnitTestBase()
        {
            _customerCoreService = new CustomerCoreService(base._context);
            _contactCoreService = new ContactCoreService(base._context);

            contactApplicationService = new ContactApplicationService(_customerCoreService, _contactCoreService);
            customerApplicationService = new CustomerApplicationService(_customerCoreService, _contactCoreService);
        }
    }
}
