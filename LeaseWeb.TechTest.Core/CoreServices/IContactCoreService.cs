﻿using LeaseWeb.TechTest.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeaseWeb.TechTest.Core.CoreServices
{
    public interface IContactCoreService
    {
        Contact AddOrUpdateContact(Contact contact);
        void DeleteContact(int contactId);
        Contact SetMainContact(int contactId, string phoneNumber = "");
        IQueryable<Contact> GetContacts(string name = "", string address = "", string email = "");
    }
}
