﻿using LeaseWeb.TechTest.DataAccess;
using LeaseWeb.TechTest.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeaseWeb.TechTest.Core.CoreServices
{
    public class CustomerCoreService : ICustomerCoreService
    {
        private Context _context;

        public CustomerCoreService(Context context)
        {
            _context = context;
        }

        public Customer AddOrUpdateCustomer(Customer customer)
        {
            //this.ValidateCustomer(customer);
            if (_context.Customers.Any(x => x.RegistrationNumber.Equals(customer.RegistrationNumber) && x.Id != customer.Id))
                throw new Exception($"Customer with registration number {customer.RegistrationNumber} already exists");

            if (customer.NumberOfEmployees < 1)
                throw new Exception("Number of employees must be at least 1");

            var existingCustomer = _context.Customers.Find(customer.Id);

            if (existingCustomer == null)
            {
                _context.Add(customer);
            }
            else
            {
                var dbEntry = _context.Entry(existingCustomer);
                dbEntry.CurrentValues.SetValues(customer);
            }

            _context.SaveChanges();

            return customer;
        }

        public void DeleteCustomer(int customerId)
        {
            var customer = _context.Customers.Find(customerId);

            if (customer == null)
                throw new Exception($"Can not delete customer with ID {customerId} because it does not exist");

            var contacts = _context.Contacts.Where(x => x.CustomerId == customer.Id);

            foreach (var item in contacts)
            {
                _context.Contacts.Remove(item);
            }

            _context.Customers.Remove(customer);

            _context.SaveChanges();
        }

        public IQueryable<Customer> GetCustomers(string name = "", string registrationNumber = "")
        {
            var customers = _context.Customers.AsQueryable<Customer>();

            if (!string.IsNullOrWhiteSpace(name))
                customers = customers.Where(x => x.CompanyName.Contains(name, StringComparison.InvariantCultureIgnoreCase));
            if (!string.IsNullOrWhiteSpace(registrationNumber))
                customers = customers.Where(x => x.RegistrationNumber.Contains(registrationNumber));

            return customers;
        }
    }
}
