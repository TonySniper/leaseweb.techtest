﻿using LeaseWeb.TechTest.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeaseWeb.TechTest.Core.CoreServices
{
    public interface ICustomerCoreService
    {
        Customer AddOrUpdateCustomer(Customer customer);
        void DeleteCustomer(int customerId);
        IQueryable<Customer> GetCustomers(string name = "", string registrationNumber = "");
    }
}
