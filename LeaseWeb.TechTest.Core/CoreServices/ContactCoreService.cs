﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LeaseWeb.TechTest.DataAccess;
using LeaseWeb.TechTest.DataAccess.Model;

namespace LeaseWeb.TechTest.Core.CoreServices
{
    public class ContactCoreService : IContactCoreService
    {
        private Context _context;

        public ContactCoreService(Context context)
        {
            _context = context;
        }

        public Contact AddOrUpdateContact(Contact contact)
        {
            this.ValidateContactRequiredFields(contact);

            var customer = _context.Customers.Find(contact.CustomerId);

            if (customer == null)
                throw new Exception($"Cannot add Contact. Customer with ID {contact.CustomerId} does not exist");

            this.ValidateContactTypes(contact);
            this.ValidateContactEmails(contact);

            var existingContact = _context.Contacts.Find(contact.Id);

            if (existingContact == null)
            {
                _context.Contacts.Add(contact);
            }
            else
            {
                var dbEntry = _context.Entry(existingContact);
                dbEntry.CurrentValues.SetValues(contact);
            }

            _context.SaveChanges();

            return contact;
        }

        public Contact SetMainContact(int contactId, string phoneNumber = "")
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var contact = _context.Contacts.Find(contactId);

                    if (!string.IsNullOrWhiteSpace(phoneNumber))
                        contact.PhoneNumber = phoneNumber;

                    this.ValidateContactRequiredFields(contact);

                    var contacts = _context.Contacts.Where(x => x.CustomerId == contact.CustomerId);

                    foreach (var item in contacts)
                    {
                        item.IsMain = false;
                        var dbEntry = _context.Entry(item);
                        dbEntry.Property(x => x.IsMain).IsModified = true;
                    }

                    contact.IsMain = true;
                    var updatedEntry = _context.Entry(contact);
                    updatedEntry.Property(x => x.IsMain).IsModified = true;

                    _context.SaveChanges();
                    transaction.Commit();

                    return contact;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        public void DeleteContact(int contactId)
        {
            var contact = _context.Contacts.Find(contactId);

            if (contact == null)
                throw new Exception($"Can not delete contact with ID {contactId} because it does not exist");

            if (contact.IsMain)
                throw new Exception("Cannot delete a contact that is marked as Main Contact");

            _context.Contacts.Remove(contact);
            _context.SaveChanges();
        }

        public IEnumerable<Contact> GetContacts(string name = "", string address = "", string email = "")
        {
            var contacts = _context.Contacts.AsQueryable();

            if (!string.IsNullOrWhiteSpace(name))
                contacts = contacts.Where(x => x.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase));

            if (!string.IsNullOrWhiteSpace(address))
                contacts = contacts.Where(x => x.Address.Contains(address, StringComparison.InvariantCultureIgnoreCase));

            if (!string.IsNullOrWhiteSpace(email))
                contacts = contacts.Where(x => x.Email.Contains(email, StringComparison.InvariantCultureIgnoreCase));

            return contacts.ToList();
        }

        private void ValidateContactTypes(Contact contact)
        {
            var contactsForCustomer = _context.Contacts.Where(x => x.CustomerId == contact.CustomerId);

            if (contactsForCustomer.Where(x => x.Id != contact.Id).All(x => x.IsMain == false) && contact.IsMain == false)
                throw new Exception("Customer must have at least 1 main contact");

            if (contactsForCustomer.Where(x => x.Id != contact.Id).Any(x => x.IsMain == true) && contact.IsMain == true)
                throw new Exception("Can not have more than one main contact");

            if (contactsForCustomer.Where(x => x.Id != contact.Id).Any(x => x.IsFinancial == true) && contact.IsFinancial == true)
                throw new Exception("Can not have more than one financial contact");

            if (contactsForCustomer.Where(x => x.Id != contact.Id).Any(x => x.IsTechnical == true) && contact.IsTechnical == true)
                throw new Exception("Can not have more than one technical contact");
        }

        private void ValidateContactEmails(Contact contact)
        {
            var contactsForCustomer = _context.Contacts
                .Where(x => !string.IsNullOrWhiteSpace(x.Email))
                .Where(x => x.CustomerId == contact.CustomerId && x.Id != contact.Id);

            if (contactsForCustomer.Any(x => x.Email.Equals(contact.Email, StringComparison.InvariantCultureIgnoreCase)))
                throw new Exception($"Email of the contact for this customer must be unique");
        }

        public void ValidateContactRequiredFields(Contact contact)
        {
            if (string.IsNullOrWhiteSpace(contact.Address) || string.IsNullOrWhiteSpace(contact.City))
                throw new Exception("Contact must have a city and an address");

            bool isPhoneNumberRequired = contact.IsFinancial || contact.IsMain || contact.IsTechnical;

            if (isPhoneNumberRequired && string.IsNullOrWhiteSpace(contact.PhoneNumber))
                throw new Exception("Phone number is required for Main, Technical and Financial contacts");
        }

        private void ValidateContact(Contact contact)
        {
            this.ValidateContactRequiredFields(contact);
            this.ValidateContactEmails(contact);
            this.ValidateContactTypes(contact);
        }

        IQueryable<Contact> IContactCoreService.GetContacts(string name, string address, string email)
        {
            var contacts = _context.Contacts.AsQueryable();

            if (!string.IsNullOrWhiteSpace(name))
                contacts = contacts.Where(x => !string.IsNullOrWhiteSpace(x.Name) && x.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase));

            if (!string.IsNullOrWhiteSpace(address))
                contacts = contacts.Where(x => x.Address.Contains(address, StringComparison.InvariantCultureIgnoreCase));

            if (!string.IsNullOrWhiteSpace(email))
                contacts = contacts.Where(x => !string.IsNullOrWhiteSpace(x.Email) && x.Email.Contains(email, StringComparison.InvariantCultureIgnoreCase));

            return contacts;
        }
    }
}
