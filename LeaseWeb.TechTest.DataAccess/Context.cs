﻿using LeaseWeb.TechTest.DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System;

namespace LeaseWeb.TechTest.DataAccess
{
    public class Context : DbContext
    {
        private readonly string _connectionString;
        private readonly bool _isUnitTest;

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        public Context(string connectionString)
        {
            _connectionString = connectionString;
            //Used due to the fact that I see no problem with having the database dropped/created for this test
            this.Database.EnsureCreated();
        }

        public Context(DbContextOptions options, bool isUnitTest = false)
            : base(options)
        {
            _isUnitTest = isUnitTest;
            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            this.ConfigureCustomerMapping(modelBuilder);
            this.ConfigureContactMapping(modelBuilder);

            if (!_isUnitTest)
                InitialSeed.Seed(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void ConfigureCustomerMapping(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Customer>().Property(x => x.RegistrationNumber).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.RegistrationNumber).HasMaxLength(10).IsRequired();
            modelBuilder.Entity<Customer>().Property(x => x.NumberOfEmployees).IsRequired();

            //modelBuilder.Entity<Customer>().HasMany<Contact>().WithOne();
        }

        private void ConfigureContactMapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>().Property(x => x.City).IsRequired();
            modelBuilder.Entity<Contact>().Property(x => x.Address).IsRequired();
            modelBuilder.Entity<Contact>().Property(x => x.CustomerId).IsRequired();

            modelBuilder.Entity<Contact>().HasOne<Customer>().WithMany().HasForeignKey(x => x.CustomerId);
        }
    }
}
