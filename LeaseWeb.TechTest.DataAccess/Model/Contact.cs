﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.DataAccess.Model
{
    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsMain { get; set; }
        public bool IsFinancial { get; set; }
        public bool IsTechnical { get; set; }
        public bool IsOther { get; set; }

        public int CustomerId { get; set; }
    }
}
