﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeaseWeb.TechTest.DataAccess.Model
{
    public class Customer
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string RegistrationNumber { get; set; }
        public string WebSite { get; set; }
        public int NumberOfEmployees { get; set; }
        public int MaxOutstandingOrderAmount { get; set; }
        public DateTime? CustomerSince { get; set; }

        //public virtual IList<Contact> Contacts { get; set; }        
    }
}
