﻿using LeaseWeb.TechTest.DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.DataAccess
{
    public static class InitialSeed
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            CreateCustomers(modelBuilder);
        }

        private static void CreateCustomers(ModelBuilder modelBuilder)
        {
            var customer = new Customer
            {
                Id = 1,
                CompanyName = "Auto Parts Limited",
                CustomerSince = DateTime.Now.Date,
                MaxOutstandingOrderAmount = 150,
                NumberOfEmployees = 10,
                RegistrationNumber = "0123456789",
                WebSite = "www.autoparts.com"
            };

            var customer2 = new Customer
            {
                Id = 2,
                CompanyName = "Booking",
                CustomerSince = DateTime.Now.Date,
                MaxOutstandingOrderAmount = 300000,
                NumberOfEmployees = 200,
                RegistrationNumber = "7418529632",
                WebSite = "www.booking.com"
            };

            var customer3 = new Customer
            {
                Id = 3,
                CompanyName = "Bradesco SA",
                CustomerSince = DateTime.Now.Date,
                MaxOutstandingOrderAmount = 75000000,
                NumberOfEmployees = 1250,
                RegistrationNumber = "9876543210",
                WebSite = "www.bradesco.com"
            };

            var customer4 = new Customer
            {
                Id = 4,
                CompanyName = "Cat Cats Limited",
                CustomerSince = DateTime.Now.Date,
                MaxOutstandingOrderAmount = 25000,
                NumberOfEmployees = 20,
                RegistrationNumber = "1235643215",
                WebSite = "www.catcats.com"
            };

            var customer5 = new Customer
            {
                Id = 5,
                CompanyName = "Dog Doggy Pet Shop",
                CustomerSince = DateTime.Now.Date,
                MaxOutstandingOrderAmount = 650,
                NumberOfEmployees = 30,
                RegistrationNumber = "9517538520",
                WebSite = "www.dogdoggy.com"
            };

            modelBuilder.Entity<Customer>().HasData(customer);
            modelBuilder.Entity<Customer>().HasData(customer2);
            modelBuilder.Entity<Customer>().HasData(customer3);
            modelBuilder.Entity<Customer>().HasData(customer4);
            modelBuilder.Entity<Customer>().HasData(customer5);

            CreateContacts(modelBuilder, customer.Id, 0, "Auto");
            CreateContacts(modelBuilder, customer2.Id, 3, "Booking");
            CreateContacts(modelBuilder, customer3.Id, 6, "Bradesco");
            CreateContacts(modelBuilder, customer4.Id, 9, "Cat");
            CreateContacts(modelBuilder, customer5.Id, 12, "Dog");
        }

        private static void CreateContacts(ModelBuilder modelBuilder, int customerId, int lastIdAdded, string contactName)
        {
            for (int i = 1; i <= 3; i++)
            {
                var dummyInt = lastIdAdded + i;

                var contact = new Contact
                {
                    Id = dummyInt,
                    Address = $"{contactName} Address {dummyInt}",
                    City = $"{contactName} City {dummyInt}",
                    Email = $"{contactName}{dummyInt}@email.com",
                    IsFinancial = i == 3 ? true : false,
                    IsMain = i == 3 ? true : false,
                    IsTechnical = i == 3 ? true : false,
                    IsOther = i != 3 ? true : false,
                    Name = $"{contactName} {dummyInt}",
                    PhoneNumber = $"+{dummyInt} 11 91324 6548",
                    CustomerId = customerId
                };

                modelBuilder.Entity<Contact>().HasData(contact);
            }
        }
    }
}
