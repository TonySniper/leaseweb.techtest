﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.ContactVM
{
    public class ContactSearchRequestVM
    {
        public string CustomerName { get; set; }
        public string RegistrationNumber { get; set; }
        public string ContactAddress { get; set; }
        public string ContactEmail { get; set; }
    }
}
