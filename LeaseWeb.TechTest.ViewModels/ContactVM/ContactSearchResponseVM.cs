﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.ContactVM
{
    public class ContactSearchResponseVM
    {
        //public int Id { get; set; }

        //public string Name { get; set; }

        //public string City { get; set; }

        //public string Address { get; set; }

        //[Display(Name = "Phone number")]
        //public string PhoneNumber { get; set; }

        //public string Email { get; set; }

        //[Display(Name = "Main contact")]
        //public bool IsMain { get; set; }

        //[Display(Name = "Financial contact")]
        //public bool IsFinancial { get; set; }

        //[Display(Name = "Technical contact")]
        //public bool IsTechnical { get; set; }

        //[Display(Name = "Other contact")]
        //public bool IsOther { get; set; }
        public ContactVM Contact { get; set; }

        public string CustomerCompanyName { get; set; }
        public string CustomerRegistrationNumber { get; set; }
    }
}
