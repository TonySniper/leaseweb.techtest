﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.ContactVM
{
    public class ContactVM
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Address { get; set; }

        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        [Display(Name = "Main contact")]
        public bool IsMain { get; set; }

        [Display(Name = "Financial contact")]
        public bool IsFinancial { get; set; }

        [Display(Name = "Technical contact")]
        public bool IsTechnical { get; set; }

        [Display(Name = "Other contact")]
        public bool IsOther { get; set; }

        public int CustomerId { get; set; }

        public void ValidateContactPhoneNumberRequired()
        {
            bool isPhoneNumberRequired = this.IsFinancial || this.IsMain || this.IsTechnical;

            if (isPhoneNumberRequired && string.IsNullOrWhiteSpace(this.PhoneNumber))
                throw new Exception("Phone number is required for Main, Technical and Financial contacts");
        }
    }
}
