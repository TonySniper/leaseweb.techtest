﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.ContactVM
{
    public class SetAsMainContactVM
    {
        public int ContactId { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
    }
}
