﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.CustomerVM
{
    public class CustomerSearchResultVM
    {
        public CustomerVM Customer { get; set; }
        public string MainContactEmail { get; set; }
        public string MainContactAddress { get; set; }
    }
}
