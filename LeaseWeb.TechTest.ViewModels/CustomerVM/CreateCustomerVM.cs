﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.CustomerVM
{
    using ContactVM;

    public class CreateCustomerVM
    {
        public ContactVM Contact { get; set; }
        public CustomerVM Customer { get; set; }

        [Required]
        [Display(Name = "Phone number")]
        public string RequiredPhoneNumber { get; set; }

        public CreateCustomerVM()
        {
            this.Contact = new ContactVM();
            this.Customer = new CustomerVM();
            Contact.IsMain = true;
        }

        public void ValidateContactPhoneNumberRequired()
        {
            bool isPhoneNumberRequired = Contact.IsFinancial || Contact.IsMain || Contact.IsTechnical;

            if (isPhoneNumberRequired && string.IsNullOrWhiteSpace(this.RequiredPhoneNumber))
                throw new Exception("Phone number is required for Main, Technical and Financial contacts");
        }
    }
}
