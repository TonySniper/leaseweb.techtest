﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.CustomerVM
{
    public class CustomerVM
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Company name")]
        public string CompanyName { get; set; }

        [Required]
        [MinLength(10, ErrorMessage = "Registration number must have 10 numbers")]
        [MaxLength(10, ErrorMessage = "Registration number must have 10 numbers")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Registration number must be numeric")]
        [Display(Name = "Registration No.")]
        public string RegistrationNumber { get; set; }

        public string WebSite { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        [Display(Name = "Number of employees")]
        public int NumberOfEmployees { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Invalid value")]
        [Display(Name = "Max outstanding order amount")]
        public int MaxOutstandingOrderAmount { get; set; }

        [Display(Name = "Customer since")]
        public DateTime? CustomerSince { get; set; }
    }
}
