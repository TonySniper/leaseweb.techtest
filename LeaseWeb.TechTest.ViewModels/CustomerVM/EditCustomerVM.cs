﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.CustomerVM
{
    using ContactVM;

    public class EditCustomerVM
    {
        public CustomerVM Customer { get; set; }
        public IEnumerable<ContactVM> Contacts { get; set; }
    }
}
