﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LeaseWeb.TechTest.ViewModels.CustomerVM
{
    public class CustomerSearchRequestVM
    {
        [Display(Name = "Company name")]
        public string CustomerName { get; set; }

        [Display(Name = "Registration number")]
        public string RegistrationNumber { get; set; }

        [Display(Name = "Contact address")]
        public string ContactAddress { get; set; }

        [Display(Name = "Contact email")]
        public string ContactEmail { get; set; }


        [Display(Name = "Contact name")]
        public string ContactName { get; set; }
    }
}
